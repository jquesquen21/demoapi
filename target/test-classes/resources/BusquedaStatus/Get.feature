Feature: Consultar la mascota modificada por estatus

  Background:
    Given url urlDemo

  @DermoApis
  Scenario Outline: Buscar mascota por estatus
    Given path "pet/findByStatus"
    And param status = "<estatus>"
    When method GET
    Then status 200
    And print response

    Examples:
      | estatus |
      | sold    |
  