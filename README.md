# KarateAPI

## Getting started

Este proyecto contine feature de pruebas automatizadas utilizando el framework Karate 
para probar APIs REST, debemos ingresar al GitLab y descargarlo.

## Prerequisites:

- Java JDK 8 o superior instalado
- Maven instalado para la gestión de dependency
- IntelliJ IDEA

## Configuration and Execution:

- Clonar el proyecto desde el repositorio:

```
    - Ingrese desde tu navegador preferido a la siguiente url: https://gitlab.com/jquesquen21/demoapi.git
    - Verifique estar posicionado en el repositorio del proyecto (main)
    - Da clic en el boton clone y copia su url
    - Create un archivo nuevo desde tu disco local c:
    - Desde tu explorador de archivo creado, abre el terminar Git 
    - Ejecute el siguiente comando: git clone https://gitlab.com/jquesquen21/demoapi.git
```
- Importar el proyecto clonado a IntelliJ IDEA

```
    - Abre IntelliJ IDEA.
    - Selecciona "File" en el menú principal y luego "Open".
    - Navega hasta el directorio del proyecto clonado y haz clic en "OK".
```

## Execute the Karate test cases

```
    - Espera a que IntelliJ IDEA importe y construya el proyecto. 
    - Verifica que todas las dependencias se hayan descargado correctamente.     
    - Navege hasta la class KarateRunner, clic derecho "Run", desde la ruta: 'src/java/resources/KarateRunner.class'
    - Desde el Terminar tambien puedes ejecutar con el siguiente comando: mvn test -Dkarate.options="--tags @DermoApis" -Dtest=KarateRunner
```

## Project Structure

```
    - 'src/java/req': Contiene un json de registro de datos general.
    - 'src/java/resurces': Contiene los casos de pruebas creados con Karate en archivos feature.
    - 'src/java/resurces': Contiene la class runnertest para la ejecución de los feature.
    - 'src/java/karate-config.js': Contiene la base url que se consume para la ejecución de las apis.
    -  pom.xml :  Archivo de configuración de Maven
```







